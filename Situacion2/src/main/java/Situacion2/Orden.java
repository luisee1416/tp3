package Situacion2;


import java.time.LocalDateTime;


public class Orden {


protected int mesa;
private LocalDateTime horaInicio;
private LocalDateTime horaCierre;
private int cantComensales;
private String nombreMozo;

private int cantidadPlatos;

private int cantidadBebidas;


private  double precioPlato;

private double precioBebida;

private int metodoPago;


public Orden( int mesa, LocalDateTime horaInicio, LocalDateTime horaCierre,
        int cantComensales, String nombreMozo,int cantidadPlatos,int cantidadBebidas,double precioPlato,double precioBebida,int metodoPago) {
   
    this.mesa = mesa;
    this.horaInicio = horaInicio;
    this.horaCierre = horaCierre;
    this.cantComensales = cantComensales;
    this.nombreMozo = nombreMozo;
    this.cantidadPlatos=cantidadPlatos;
    this.cantidadBebidas=cantidadBebidas;
     this.precioPlato=precioBebida; /// dar valor en test
     this.precioBebida=precioPlato; 
    this.metodoPago=metodoPago;


}



public int getMesa() {
    return mesa;
}

public void setMesa(int mesa) {
    this.mesa = mesa;
}

public LocalDateTime getHoraInicio() {
    return horaInicio;
}

public void setHoraInicio(LocalDateTime horaInicio) {
    this.horaInicio = horaInicio;
}

public LocalDateTime getHoraCierre() {
    return horaCierre;
}

public void setHoraCierre(LocalDateTime horaCierre) {
    this.horaCierre = horaCierre;
}

public int getCantComensales() {
    return cantComensales;
}

public void setCantComensales(int cantComensales) {
    this.cantComensales = cantComensales;
}

public String getNombreMozo() {
    return nombreMozo;
}

public void setNombreMozo(String nombreMozo) {
    this.nombreMozo = nombreMozo;



}



public int getCantidadPlatos() {
    return cantidadPlatos;
}

public void setCantidadPlatos(int cantidadPlatos) {
    this.cantidadPlatos = cantidadPlatos;
}

public int getCantidadBebidas() {
    return cantidadBebidas;
}

public void setCantidadBebidas(int cantidadBebidas) {
	this.cantidadBebidas = cantidadBebidas;
}

public double getPrecioPlato() {
    return precioPlato;
}

public void setPrecioPlato(double precioPlato) {
    this.precioPlato = precioPlato;
}

public double getPrecioBebida() {
    return precioBebida;
}

public void setPrecioBebida(double precioBebida) {
    this.precioBebida = precioBebida;
}

public int getMetodoPago() {
    return metodoPago;
}

public void setMetodoPago(int metodoPago) {
    this.metodoPago = metodoPago;
}





}