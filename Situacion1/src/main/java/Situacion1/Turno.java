package Situacion1;


import java.time.Duration;


import java.time.LocalDateTime;

public class Turno {

private Empleado[] empleadoTurno;
private LocalDateTime fechaHoraDeIngreso;
private LocalDateTime fechaHoraDeEgreso;

private final int  TAMANIOEMPLEADO=10;



public Turno (LocalDateTime vfechaHoraDeIngreso,LocalDateTime vfechaHoraDeEgreso){

    
    this.fechaHoraDeIngreso = vfechaHoraDeIngreso;
    this.fechaHoraDeEgreso = vfechaHoraDeEgreso;
    
    empleadoTurno=new Empleado [TAMANIOEMPLEADO];


}

public LocalDateTime getFechaHoraDeIngreso() {
    return fechaHoraDeIngreso;
}

public void setFechaHoraDeIngreso(LocalDateTime fechaHoraDeIngreso) {
    this.fechaHoraDeIngreso = fechaHoraDeIngreso;
}

public LocalDateTime getFechaHoraDeEgreso() {
    return fechaHoraDeEgreso;
}

public void setFechaHoraDeEgreso(LocalDateTime fechaHoraDeEgreso) {
    this.fechaHoraDeEgreso = fechaHoraDeEgreso;
}

public Empleado[] getEmpleadoTurno() {
    return empleadoTurno;
}

public void setEmpleadoTurno(Empleado[] empleadoTurno) {
    this.empleadoTurno = empleadoTurno;
}


public long calcularHorasTrabajadas(LocalDateTime fechaHoraDeIngreso,LocalDateTime fechaHoraDeEgreso){


long resultado= Duration.between(fechaHoraDeEgreso,fechaHoraDeIngreso).toHours();


return resultado;
}






}